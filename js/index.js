$(function(){
    $("[data-bs-toggle='tooltip']").tooltip();
    $("[data-bs-toggle='popover']").popover();
    $('.carousel').carousel({
      interval:2000
    });
    $('#contacto').on('show.bs.modal',function (e){
      console.log('el modal se está mostrando')
      $("#contactoBtn").removeClass('btn-outline-success')
      $("#contactoBtn").addClass('btn-outline-warning')
    });
    $('#contacto').on('hide.bs.modal',function (e){
      console.log('el modal se oculta')
    });
});